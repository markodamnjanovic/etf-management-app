package com.etf.controllers;

import com.etf.dto.UserChangePasswordData;
import com.etf.dto.UserData;
import com.etf.entities.User;
import com.etf.factory.UserFactory;
import com.etf.service.UserService;
import com.etf.validation.aspect.ValidateRequest;
import com.etf.validation.validators.UserChangePasswordValidator;
import com.etf.validation.validators.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
//@CrossOrigin //TODO is this (and proxy-config.json) required after enabling csrf and cors in spring security?
@RequestMapping("/api/users")
public class UserController {
  @Autowired
  UserService userService;
  @Autowired
  UserFactory userFactory;

  @PostMapping(value = "/register")
  @ValidateRequest(validatorClass = UserValidator.class, dataClass = UserData.class)
  public ResponseEntity<Object> createUser(@RequestBody @Valid UserData userData, BindingResult result) {
    User user = userFactory.createUser(userData);
    userService.create(user);
    return new ResponseEntity<>(userData, HttpStatus.OK);
  }

  @PutMapping(value = "/changePassword")
  @ValidateRequest(validatorClass = UserChangePasswordValidator.class, dataClass = UserChangePasswordData.class)
  public ResponseEntity<Object> changePassword(@RequestBody @Valid UserChangePasswordData changePasswordData, BindingResult result) {
    userService.updatePassword(changePasswordData.getUsername(), changePasswordData.getPassword());
    return new ResponseEntity<>(HttpStatus.OK);
  }


  //TODO pagination on GET?
}
