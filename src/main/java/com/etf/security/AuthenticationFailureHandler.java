package com.etf.security;

import com.etf.exceptions.UserNotEnabledException;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class AuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

  public AuthenticationFailureHandler() {
  }

  public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException {
    Throwable cause = exception.getCause();
    if (cause instanceof BadCredentialsException || cause instanceof UserNotEnabledException) {
      response.setStatus(HttpStatus.FORBIDDEN.value());
      response.setCharacterEncoding(StandardCharsets.UTF_8.toString());
      response.getWriter().write(cause.getMessage());
    } else {
      response.setStatus(HttpStatus.UNAUTHORIZED.value());
    }
  }
}
