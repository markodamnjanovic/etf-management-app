package com.etf.dto;

import javax.validation.constraints.NotBlank;

public class UserChangePasswordData implements BaseData {
  @NotBlank
  private String username;

  @NotBlank
  private String password;

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }
}
