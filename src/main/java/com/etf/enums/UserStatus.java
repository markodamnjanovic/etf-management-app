package com.etf.enums;

public enum UserStatus {

  ENABLED,
  DISABLED,
  REJECTED;
}
