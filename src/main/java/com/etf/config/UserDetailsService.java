package com.etf.config;

import com.etf.entities.User;
import com.etf.enums.UserStatus;
import com.etf.exceptions.UserNotEnabledException;
import com.etf.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

/**
 * Authenticate a user from the database.
 */
@Component
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private static final Logger LOG = LoggerFactory.getLogger(UserDetailsService.class);

    @Autowired
    private UserService userService;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) {
        LOG.debug("Authenticating {}", username);
        User user = userService.findByUsername(username).orElseThrow(() -> new BadCredentialsException("User not found"));
        if (user.getStatus() != UserStatus.ENABLED) {
            throw new UserNotEnabledException("User not enabled");
        }
        return createSpringSecurityUser(user);

    }

    private org.springframework.security.core.userdetails.User createSpringSecurityUser(User user) {
      /*  List<GrantedAuthority> grantedAuthorities = user.getAuthorities().stream()
            .map(authority -> new SimpleGrantedAuthority(authority.getName()))
            .collect(Collectors.toList());*/
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), Collections.emptyList());
    }
}
