package com.etf.service.impl;

import com.etf.dao.UserDao;
import com.etf.entities.User;
import com.etf.exceptions.ApplicationException;
import com.etf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

  @Autowired
  private UserDao userDao;
  @Autowired
  private PasswordEncoder passwordEncoder;

  @Override
  public void create(User user) {
    userDao.save(user);
  }

  @Override
  public Optional<User> findByUsername(String username) {
    return userDao.findByUsername(username);
  }

  @Override
  public Optional<User> findByEmail(String email) {
    return userDao.findByEmail(email);
  }

  @Override
  public void updatePassword(String username, String newPassword) {
    User user = userDao.findByUsername(username).orElseThrow(() -> new ApplicationException("User with username " + username + " not found"));
    user.setPassword(passwordEncoder.encode(newPassword));
  }
}
