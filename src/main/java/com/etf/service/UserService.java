package com.etf.service;

import com.etf.entities.User;

import java.util.Optional;

public interface UserService {

  void create(User user);

  Optional<User> findByUsername(String username);

  Optional<User> findByEmail(String email);

  void updatePassword(String username, String newPassword);
}
