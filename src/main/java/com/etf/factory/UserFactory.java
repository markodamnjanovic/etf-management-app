package com.etf.factory;

import com.etf.dto.UserData;
import com.etf.entities.User;
import com.etf.enums.UserStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserFactory {

  @Autowired
  PasswordEncoder passwordEncoder;

  public User createUser(UserData userData) {
    User user = new User();
    user.setUsername(userData.getUsername());
    user.setFirstName(userData.getFirstName());
    user.setLastName(userData.getLastName());
    user.setBirthday(userData.getBirthday());
    user.setPhoneNumber(userData.getPhoneNumber());
    user.setEmail(userData.getEmail());
    user.setPassword(passwordEncoder.encode(userData.getPassword()));
    user.setStatus(UserStatus.DISABLED);
    return user;
  }
}
