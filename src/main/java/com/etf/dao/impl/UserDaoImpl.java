package com.etf.dao.impl;

import com.etf.dao.UserDao;
import com.etf.entities.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.Optional;

@Repository
@Transactional(value = Transactional.TxType.MANDATORY)
public class UserDaoImpl extends BaseDao<User> implements UserDao {
  @PersistenceContext
  private EntityManager em;

  @Override
  public void save(User user) {
    em.persist(user);
  }

  @Override
  public Optional<User> findByUsername(String username) {
    TypedQuery<User> query = em.createNamedQuery("User.findByUuid", User.class);
    query.setParameter("username", username);
    return findOptionalResult(query);

  }

  @Override
  public Optional<User> findByEmail(String email) {
    TypedQuery<User> query = em.createNamedQuery("User.findByEmail", User.class);
    query.setParameter("email", email);
    return findOptionalResult(query);
  }
}
