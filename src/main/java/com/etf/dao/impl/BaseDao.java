package com.etf.dao.impl;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.Optional;

public abstract class BaseDao<T> {

  public Optional<T> findOptionalResult(TypedQuery<T> query) {
    Optional<T> result;
    try {
      result = Optional.of(query.getSingleResult());
    } catch (NoResultException e) {
      result = Optional.empty();
    }
    return result;
  }
}
