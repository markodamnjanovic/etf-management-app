package com.etf.dao;

import com.etf.entities.User;

import java.util.Optional;

public interface UserDao {

  void save(User user);

  Optional<User> findByUsername(String username);

  Optional<User> findByEmail(String email);
}
