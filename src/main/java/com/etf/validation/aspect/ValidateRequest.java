package com.etf.validation.aspect;

import com.etf.dto.BaseData;
import com.etf.validation.validators.DataValidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ValidateRequest {

  Class<? extends DataValidator<? extends BaseData>> validatorClass();

  Class<? extends BaseData> dataClass();
}