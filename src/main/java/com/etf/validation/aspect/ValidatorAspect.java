package com.etf.validation.aspect;

import com.etf.config.SpringContextHolder;
import com.etf.dto.BaseData;
import com.etf.exceptions.ApplicationException;
import com.etf.validation.validators.DataValidator;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import java.util.Arrays;

@Aspect
@Component
public class ValidatorAspect {

  @Around("@annotation(com.etf.validation.aspect.ValidateRequest)")
  @SuppressWarnings("unchecked")
  public Object validateDataAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
    ValidateRequest validateAnnotation = ((MethodSignature) joinPoint.getSignature()).getMethod().getAnnotation(ValidateRequest.class);
    Class<? extends DataValidator<? extends BaseData>> validatorClass = validateAnnotation.validatorClass();
    Class<? extends BaseData> dataClass = validateAnnotation.dataClass();

    Object[] methodArguments = joinPoint.getArgs();
    BaseData data = Arrays.stream(methodArguments).filter(methodArg -> methodArg.getClass().isAssignableFrom(dataClass)).map(dataClass::cast).findFirst()
            .orElseThrow(() -> new ApplicationException("No required argument type found in intercepted method: " + dataClass));
    BindingResult result = Arrays.stream(methodArguments).filter(methodArg -> methodArg instanceof BindingResult).map(BindingResult.class::cast).findFirst()
            .orElseThrow(() -> new ApplicationException("No BindingResult method argument found in intercepted method"));

    DataValidator dataValidator = SpringContextHolder.getBean(validatorClass);
    dataValidator.validate(data, result);
    return result.hasErrors() ? new ResponseEntity<>(result.getAllErrors(), HttpStatus.BAD_REQUEST) : joinPoint.proceed();
  }
}
