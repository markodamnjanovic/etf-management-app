package com.etf.validation.validators;

import com.etf.dto.UserData;
import com.etf.entities.User;
import com.etf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import java.util.Optional;

@Component
public class UserValidator implements DataValidator<UserData> {

  @Autowired
  UserService userService;

  public void validate(UserData userData, BindingResult result) {
    Optional<User> userByUsername = userService.findByUsername(userData.getUsername());
    Optional<User> userByEmail = userService.findByEmail(userData.getEmail());
    userByUsername.ifPresent(u -> result.reject("username", "Username is already in use"));
    userByEmail.ifPresent(u -> result.reject("email", "E-mail is already in use"));
  }
}
