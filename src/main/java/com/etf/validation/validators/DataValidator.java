package com.etf.validation.validators;

import com.etf.dto.BaseData;
import org.springframework.validation.BindingResult;

public interface DataValidator<T extends BaseData> {

  void validate(T data, BindingResult result);
}
