package com.etf.validation.validators;

import com.etf.dto.UserChangePasswordData;
import com.etf.entities.User;
import com.etf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import java.util.Optional;

@Component
public class UserChangePasswordValidator implements DataValidator<UserChangePasswordData> {

  @Autowired
  UserService userService;
  @Autowired
  PasswordEncoder passwordEncoder;

  public void validate(UserChangePasswordData userChangePasswordData, BindingResult result) {
    Optional<User> user = userService.findByUsername(userChangePasswordData.getUsername());
    if (!user.isPresent()) {
      result.reject("username", "Username doesn't exist");
    } else if (passwordEncoder.matches(userChangePasswordData.getPassword(), user.get().getPassword())) {
      result.reject("username", "User already uses this password");
    }
  }
}