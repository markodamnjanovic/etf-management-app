package com.etf.entities;

import com.etf.enums.UserStatus;

import javax.persistence.*;
import java.time.LocalDate;

@Entity(name = "etf.user")
@Table(name = "etf_user")
@NamedQueries({
        @NamedQuery(name = "User.findByUuid", query = "FROM etf.user where username = :username"),
        @NamedQuery(name = "User.findByEmail", query = "FROM etf.user where email = :email")
})
public class User extends BaseEntity {
  @Column(nullable = false, unique = true)
  private String username;

  @Column(nullable = false)
  private String firstName;

  @Column(nullable = false)
  private String lastName;

  @Column(nullable = false)
  private String password;

  @Column(nullable = false)
  private LocalDate birthday;

  @Column(nullable = false)
  private String phoneNumber;

  @Column(nullable = false, unique = true)
  private String email;

  @Column
  UserStatus status;

  public String getUsername() {
    return username;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getPassword() {
    return password;
  }

  public LocalDate getBirthday() {
    return birthday;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public String getEmail() {
    return email;
  }

  public UserStatus getStatus() {
    return status;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setBirthday(LocalDate birthday) {
    this.birthday = birthday;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setStatus(UserStatus status) {
    this.status = status;
  }

  public static class UserBuilder {
    //TODO implement builder and remove setters
  }
}
