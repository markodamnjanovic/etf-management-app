package com.etf.entities;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.UUID;

@MappedSuperclass
public abstract class BaseEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column
  @NotBlank
  private String uuid = UUID.randomUUID().toString();

  public Long getId() {
    return id;
  }

  public String getUuid() {
    return uuid;
  }
}
