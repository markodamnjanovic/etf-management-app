export class UserModel {
  constructor(public username: string, public firstName: string, public lastName: string, public password: string, public birthday: string,
    public phoneNumber: string, public email: string,) {}
}
