import {Component, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import {HttpErrorResponse, HttpEvent, HttpHeaders} from "@angular/common/http";
import {HttpService} from "../../../services/HttpService";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit { //TODO rename to login and move to auth

  showChangePassword: boolean = false;
  loginErrorMessage: string = '';

  constructor(private httpService: HttpService) { }

  ngOnInit() {
  }

  onLogin(form: NgForm) {
    if (form.valid) {
      const credentials = 'j_username=' + encodeURIComponent(form.value.username) + '&j_password=' + encodeURIComponent(form.value.password);
      const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
      this.httpService.post('/api/users/login', credentials, headers).subscribe(
        (response: HttpEvent<Object>) => {
          if (response.type !== 0) { // because of CORS, OPTIONS request is sent before POST and it returns OK with type 0
           // this.registerSuccessful.emit();
          }
        },
        (error: HttpErrorResponse) => {
          if (error.status !== 500 && error.status !== 401) {
            this.loginErrorMessage = error.error;
          }
        }
      );
    }
  }

  toggleChangePassword(value: boolean) {
    this.showChangePassword = value;
  }

}
