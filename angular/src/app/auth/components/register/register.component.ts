import {Component, Injectable, Output} from '@angular/core';
import {UserModel} from "../../../models/UserModel";
import {NgForm} from "@angular/forms";
import {AuthService} from "../../services/AuthService";
import {HttpErrorResponse, HttpEvent} from "@angular/common/http";
import {HttpService} from "../../../services/HttpService";
import {Subject} from "rxjs";
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html'
})
@Injectable()
export class RegisterComponent {

  registerErrorMessage: string = '';

  @Output() registerSuccessful: Subject<any> = new Subject<any>();

  constructor(private authService: AuthService, private httpService: HttpService) { }

  onRegister(form: NgForm) {
    if (form.valid && form.value.password === form.value.confirmPassword) {
      const userModel = this.createUser(form);
      this.httpService.post('/api/users/register', userModel).subscribe(
        (response: HttpEvent<Object>) => {
          if (response.type !== 0) { // because of CORS, OPTIONS request is sent before POST and it returns OK with type 0.
            this.authService.setAuthenticated(true);
            this.registerSuccessful.next();
          }
        },
        (error: HttpErrorResponse) => {
          if (error.status !== 500 && error.status !== 401) {
            this.registerErrorMessage = error.error.map(error => error.defaultMessage).join('; ');
          }
        }
      );
    }
  }

  private createUser(form: NgForm): UserModel {
    const username: string = form.value.username;
    const firstName: string = form.value.firstName;
    const lastName: string = form.value.lastName;
    const password: string = form.value.password;
    const birthday: string = form.value.birthday;
    const phone: string = form.value.phoneNumber;
    const email: string = form.value.email;
    return new UserModel(username, firstName, lastName, password, birthday, phone, email)
  }
}
