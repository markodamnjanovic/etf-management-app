import {Injectable} from "@angular/core";
import {HttpService} from "../../services/HttpService";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";

@Injectable()
export class AuthService {

  private authenticated: boolean = false; //TODO see how to hold session when user logs in. Implement logout and clear session.

  setAuthenticated(authenticated: boolean) {
    this.authenticated = authenticated;
  }

  getAuthenticated() {
    return this.authenticated;
  }

  constructor(private httpService: HttpService, private toast: ToastrService, private router: Router) {}
}
