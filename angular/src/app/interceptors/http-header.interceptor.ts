import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import 'rxjs/add/operator/do';
import {CookieService} from "ngx-cookie-service";

@Injectable()
export class HttpHeaderInterceptor implements HttpInterceptor {

  private readonly HTTP_METHODS_WITH_XSRF_TOKEN: string[] = ['POST', 'PUT', 'DELETE'];

  constructor(private cookieService: CookieService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const setXsrfHeader = this.HTTP_METHODS_WITH_XSRF_TOKEN.indexOf(request.method) !== -1;
    if (setXsrfHeader) {
      const requestWithXsrfToken = request.clone({withCredentials: true, headers: request.headers.append('X-XSRF-TOKEN', this.cookieService.get("XSRF-TOKEN"))});
      return next.handle(requestWithXsrfToken)
    } else {
      return next.handle(request);
    }
  }
}
