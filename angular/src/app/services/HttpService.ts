import {Injectable} from "@angular/core";
import {HttpClient, HttpErrorResponse, HttpEvent, HttpHeaders, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

@Injectable()
export class HttpService {

  static readonly BASE_URL = 'http://localhost:8080';

  constructor(private httpClient: HttpClient, private router: Router, private toast: ToastrService) {}

  post(url: string, requestBody: any, headers?: HttpHeaders): Observable<HttpEvent<any>> {
    const request = new HttpRequest('POST', HttpService.BASE_URL + url, requestBody, {headers: headers});
    return this.httpClient.request(request);
  }

  put(url: string, requestBody: any): Observable<HttpEvent<any>> {
    const request = new HttpRequest('PUT', HttpService.BASE_URL + url, requestBody);
    return this.httpClient.request(request);
  }

  get(url): Observable<HttpEvent<any>> {
    const request = new HttpRequest('GET', HttpService.BASE_URL + url);
    return this.httpClient.request(request);
  }

  handleErrorResponse(error: HttpErrorResponse) {
    if (error.error) {
      if (error.status === 500) {
        this.toast.error(error.error.message, 'Internal server error!')
      } else if (error.status === 401) {
        this.router.navigate(['/unauthorized']);
      }
    }
  }
}
