import {Output} from "@angular/core";
import {Subject} from "rxjs";

export class HeaderService {

  @Output() showSignInUp = new Subject<boolean>(); // call next() on this in ngOnInit of components where sign in/up on header should be visible

  constructor() {}

  toggleShowSignInUp(value: boolean) {
    this.showSignInUp.next(value);
  }
}
