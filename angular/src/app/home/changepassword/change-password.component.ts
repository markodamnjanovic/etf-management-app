import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NgForm} from "@angular/forms";
import {HttpEvent} from "@angular/common/http";
import {HttpService} from "../../services/HttpService";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html'
})
export class ChangePasswordComponent implements OnInit {

  @Output() closeChangePassword: EventEmitter<any> = new EventEmitter<any>();

  constructor(private httpService: HttpService, private toast: ToastrService) { }

  ngOnInit() {
  }

  onChangePassword(form: NgForm) {
    if (form.valid && form.value.password === form.value.confirmPassword) {
      const requestData = {username: form.value.username, password: form.value.confirmPassword };
      this.httpService.put('/api/users/changePassword', requestData).subscribe(
        (response: HttpEvent<Object>) => {
          if (response.type != 0) {
            this.toast.success('Password changed successfully!');
            this.closeChangePassword.emit();
          }
        }
      )
    }
  }

}
