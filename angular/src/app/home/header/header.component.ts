import {Component, OnDestroy, OnInit} from '@angular/core';
import {HeaderService} from "../../services/HeaderService";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit, OnDestroy {

  showSignInUp: boolean = false;

  constructor(private headerService: HeaderService) {}

  ngOnInit(): void {
    this.headerService.showSignInUp.subscribe(
      (value: boolean) => {
        this.showSignInUp = value;
      }
    );
  }

  ngOnDestroy(): void {
    this.headerService.showSignInUp.unsubscribe();
  }
}
