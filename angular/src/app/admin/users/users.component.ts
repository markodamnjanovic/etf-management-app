import {Component, OnDestroy, OnInit} from '@angular/core';
import {HeaderService} from "../../services/HeaderService";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html'
})
export class UsersComponent implements OnInit, OnDestroy {

  constructor(private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.toggleShowSignInUp(true);
  }

  ngOnDestroy() {
    this.headerService.toggleShowSignInUp(false); //TODO maybe better way to do this is to have a list of pages that are condition for showing the links (and check the current url with Router).
                                                  //TODO also check if user is signed in in the same place
  }
}
