import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {FormsModule} from '@angular/forms';
import {HeaderComponent} from './home/header/header.component';
import {HttpService} from "./services/HttpService";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {FieldErrorComponent} from './validation/field-error/field-error.component';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from "./auth/components/login/login.component";
import {HttpErrorInterceptor} from "./interceptors/http-error.interceptor";
import {ChangePasswordComponent} from "./home/changepassword/change-password.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ToastrModule} from "ngx-toastr";
import {HeaderService} from "./services/HeaderService";
import {UsersComponent} from './admin/users/users.component';
import {CookieService} from "ngx-cookie-service";
import {HttpHeaderInterceptor} from "./interceptors/http-header.interceptor";
import {AuthService} from "./auth/services/AuthService";
import {RegisterComponent} from "./auth/components/register/register.component";
import {UnauthorizedComponent} from './auth/components/unauthorized/unauthorized.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    FieldErrorComponent,
    HomeComponent,
    ChangePasswordComponent,
    UsersComponent,
    UnauthorizedComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
  ],
  providers: [HttpService, CookieService,  HeaderService, AuthService, {provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: HttpHeaderInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
